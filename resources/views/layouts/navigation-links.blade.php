<li class="nav-item active">
    <a class="nav-link" href="./">
        <span class="nav-link-icon d-md-none d-lg-inline-block">
            <x-icon type="home" classicon="icon" />
        </span>
        <span class="nav-link-title">
            Home
        </span>
    </a>
</li>
